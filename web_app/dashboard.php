<?php

use App\Model\Employee;

/**
 * Handles main page
 *
 * @author Sherwin Ocubillo <sher_ocs@yahoo.com>
 * @version 1
 * @package FlexioutsourceIT
 */
require 'bootstrap.php';

class Dashboard extends Controller
{
    protected $className = 'index';

    protected function indexAction($param)
    {
        /**
         * @todo
         *
         * Verify this page, if the user can access.
         */

        echo $this->render('index/dashboard.phtml', $param);
    }
}

$class = new Dashboard($_REQUEST);

if (isset($_REQUEST['m'])) {
    $method = $_REQUEST['m'];
    $class->$method($_REQUEST);
} else {
    $class->index($_REQUEST);
}
