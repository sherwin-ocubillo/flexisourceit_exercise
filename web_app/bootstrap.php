<?php

define('APPLICATION_ENV', getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'local');
define('BASE_PATH', realpath(dirname(__FILE__)));
define('APPLICATION_PATH', BASE_PATH.'/../app/');
define('VENDOR_PATH', BASE_PATH.'/../vendor/');
define('CONFIG_PATH', APPLICATION_PATH.'config');

define('ZEND_VERSION', '1.11.12');  // change zend version accordingly
define('ZEND_PATH', VENDOR_PATH.'zend/'.ZEND_VERSION);

set_include_path(ZEND_PATH.PATH_SEPARATOR.get_include_path());

if (APPLICATION_ENV == 'production') {
    ini_set('display_errors', 0);
} else {
    ini_set('display_errors', 1);
}

/**
 *  Autoload the Zend core libraries
 */
require_once ZEND_PATH.'/Zend/Loader/Autoloader.php';
Zend_Loader_Autoloader::getInstance();

/**
 *  Starting Session
 */
Zend_Session::start();

/**
 *  Set up the application configuration file
 */
$config = new Zend_Config_Ini(CONFIG_PATH.'/web_app.ini', APPLICATION_ENV );
Zend_Registry::set("config", $config);

/**
 *  Setup view application object
 */
$view = new Zend_View(array('basePath' => APPLICATION_PATH . 'views'));
Zend_Registry::set("view", $view);

/**
 *  Register Classes Autoloader
 */
// add autoload from composer to load Snappy
require VENDOR_PATH . '/autoload.php';
spl_autoload_register('myAutoloader');
function myAutoloader($className)
{
    $arrSegment = explode('_', $className);

    if (empty($arrSegment)) return false;

    $className = $arrSegment[count($arrSegment)-1];  // E.g. Appointment

    if( stripos($className, '\\') > -1 ) {
        $arrSegment2 = explode('\\', $className);
        $className = $arrSegment2[count($arrSegment2)-1];
        unset($arrSegment2[count($arrSegment2)-1]); // Taking out class name
        $strDirectory = implode('/', $arrSegment2);

        include VENDOR_PATH . '/aws/' . $strDirectory . '/' . $className . '.php';

    } else {

        unset($arrSegment[count($arrSegment)-1]); // Taking out class name

        $arrDirectory = array_map('strtolower', $arrSegment);
        /* Dirty hack, add character 's' at the end of folder e.g. model => models  */
        $arrDirectory[0] = $arrDirectory[0].'s';
        $strDirectory = implode('/', $arrDirectory);

        include APPLICATION_PATH.$strDirectory.'/'.$className.'.php';
    }
}

/**
 *  Initialize Database connnection
 */
//$dbAdapter = Zend_Db::factory($config->database);
$dbAdapter = new Adapter_PdoMysql($config->database->params);
Zend_Db_Table::setDefaultAdapter($dbAdapter);

/**
 * Include controller.php
 */
require APPLICATION_PATH . 'controller.php';