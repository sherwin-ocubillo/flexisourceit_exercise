<?php

use App\Model\Employee;

/**
 * Handles Api Request
 *
 * @author Sherwin Ocubillo <sher_ocs@yahoo.com>
 * @version 1
 * @package FlexioutsourceIT
 */
require 'bootstrap.php';

class Api extends Controller
{
    protected $className = 'api';

    protected function saveEmployeeAction($param)
    {
        /**
         * @todo
         *
         * Do all parameters request validation
         */

        $employee_model = new Employee();

        $employee_model->getTable()->setName($param['name']);
        $employee_model->getTable()->setPhoneNumber($param['phone_number']);
        $employee_model->getTable()->setEmail($param['email']);
        $employee_model->getTable()->setType($param['type']);

        /**
         * @todo
         *
         * Temporary password generation
         */
        $generatedPassword = uniqid();
        $employee_model->password = sha1($generatedPassword);

        $employee_model->save();

        /**
         * @todo
         *
         * Trigger event observer for sending email
         */

        echo json_encode([
            "status" => "success",
            "message" => $employee_model->id . " was added"
        ]);
    }
}

$class = new Api($_REQUEST);

if (isset($_REQUEST['m'])) {
    $method = $_REQUEST['m'];
    $class->$method($_REQUEST);
} else {
    $class->index($_REQUEST);
}
