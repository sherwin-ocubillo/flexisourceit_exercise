<?php
/**
 * Controller to handle request/response.
 *
 * @author Sherwin Ocubillo <sher_ocs@yahoo.com>
 * @version 1
 * @package FlexioutsourceIT
 */
class Controller
{
	/**
	 *  string $error
	 */
	public $error;
	/**
	 *  $_SESSION $user
	 */
	public $user;
	/**
	 *  Zend_View $view
	 */
	public $view;
	/**
	 *  Zend_Config_Ini $config
	 */
	public $config;
	/**
	 *  $_REQUEST
	 */
	protected $_param;

    protected $_guest_class = array('Index');

    /**
     * @var array
     */
    protected $_observers;

	public function __construct($param = array())
	{
		if (Zend_Registry::isRegistered("view")) {
			$this->view = Zend_Registry::get("view");
		}
		if (Zend_Registry::isRegistered("config")) {
			$this->config = Zend_Registry::get("config");
		}
		
		$this->_param = $param;

        try
        {
            /**
             * Allow classes that is not to be authenticated
             */
            if (!in_array(get_class($this), $this->_guest_class)) {
                $this->_initUser();
            }

            /**
             * Register an observer to send email.
             */
            $this->_registerObserver(new App\Observer\EmailSender);
        }
        catch (Exception $err)
        {
            if ($this->className != 'index') {
                $return = array('success' => false, 'message' => $err->getMessage());
                echo json_encode($return); exit();
            } else {
                header('location: new.php');
            }
        }
	}

    public function __call($method, $args)
    {
        $internalMethod = lcfirst($method).'Action';

        if (method_exists($this, $internalMethod)) {
            try {
                call_user_func_array(array($this, $internalMethod), array($this->_param));
            }catch(Exception $err) {
                /**
                 * @todo
                 * Log error in a restricted file.
                 */
                echo $err->getMessage();
            }
        }
    }

	protected function render($file, $param = array(), $resource=NULL)
	{
        if (!empty($param)) {
            foreach($param as $key => $value) {
                $this->_sanitizeViewParamKey($key);
                $this->view->$key = $value;
            }
        }

		return $this->view->render($file);
	}

    private function _sanitizeViewParamKey(&$key)
    {
        /**
         * @todo
         *
         * Do all intensive view parameter sanitation
         */
        if (preg_match('/^_(.*)/', $key)) {
            $key = str_replace('_', '', $key, $count = 1);
        }
    }

    protected function _registerObserver($observer)
    {
        if ($observer instanceof Observer_Interface) {
            $this->_observers[] = $observer;
        }
    }

    private function _initUser()
    {
        /**
         * @todo
         *
         * Initialize valid user
         */
    }
}
