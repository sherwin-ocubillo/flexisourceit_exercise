<?php namespace App\Model;

/**
 * Class Employee
 * @package App\Model
 */
class Employee
{	
	protected $_table;
	
	protected $_select;
	
	public function __construct()
	{
		$this->_table = new App\Model\Table\Employee();
	}
	
	public function getTable()
	{	
		return $this->_table;
	}

	public function save()
    {
        /**
         * @todo
         *
         * Check here if the record exist then do an update otherwise do an insert operation.
         */
    }
}
