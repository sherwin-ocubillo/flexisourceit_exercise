<?php namespace App\Model;

class Table extends Zend_Db_Table implements Countable
{
    /**
     * Fetches all rows into an array (thus without creating Zend_Db_Table_Row instances).
     *
     * @param Zend_Db_Table_Select $select The select statement
     * @param int $count The number of rows to return.
     * @param int $offset Start returning after this many rows.

     * @return array The query result as array
     */
    public function fetchAllToArray(Zend_Db_Table_Select $select = null, $count = null, $offset = null)
    {
        /**
         * @todo
         *
         * Fetching all records
         */
    }


    /**
     * Fetches one rows as an array (thus without creating a Zend_Db_Table_Row instance).
     *
     * @param Zend_Db_Table_Select $select The select statement
     * @return array The result row as array
     */
    public function fetchRowToArray(Zend_Db_Table_Select $select)
    {
        /**
         * @todo
         *
         * Fetching one record
         */
    }

}